#include "data.h"

void iniciaData(void)
{
 
  GPIOInit();
  init_timer32(0, 10);
  SSPInit();
   
  
}
int verificaSD()

{
     
  DSTATUS stat;
  DWORD p2;
  WORD w1;
  BYTE res, b1;
  DIR dir;  
 
  int i = 0;

   stat = disk_initialize(0);
    if (stat & STA_NOINIT) {
        sprintf((char*)bufData, "MMC: not initialized\r\n");
    }
    
    if (stat & STA_NODISK) {
        sprintf((char*)bufData, "MMC: no Disk\r\n");        
    }
    
    if (stat != 0) {
        return 1;
    }
    
    //UARTSendString((uint8_t*)"MMC: Initialized\r\n");
    
    if (disk_ioctl(0, GET_SECTOR_COUNT, &p2) == RES_OK) {
        i = sprintf((char*)bufData, "Drive size: %d \r\n", p2);
        
    }
    
    if (disk_ioctl(0, GET_SECTOR_SIZE, &w1) == RES_OK) {
        i = sprintf((char*)bufData, "Sector size: %d \r\n", w1);
        
    }
    
    if (disk_ioctl(0, GET_BLOCK_SIZE, &p2) == RES_OK) {
        i = sprintf((char*)bufData, "Erase block size: %d \r\n", p2);
        
    }
    
    if (disk_ioctl(0, MMC_GET_TYPE, &b1) == RES_OK) {
        i = sprintf((char*)bufData, "Card type: %d \r\n", b1);
        
    }
    
    res = f_mount(0, &Fatfs[0]);
    
    if (res != FR_OK) {
        i = sprintf((char*)bufData, "Failed to mount 0: %d \r\n", res);
        
        return 1;
    }
    
    res = f_opendir(&dir, "/");
    if (res) {
        i = sprintf((char*)bufData, "Failed to open /: %d \r\n", res);
        
        return 1;
    }
    
    for(;;) {
        res = f_readdir(&dir, &Finfo);
        if ((res != FR_OK) || !Finfo.fname[0]) break;   
        
    } 
    
  
}


int criaArquivo()
{

     fr = f_open(&fil, "data.csv", FA_WRITE | FA_CREATE_ALWAYS | FA_READ);  //Fun�ao para abrir arquivo
      if (fr) return (int)fr;//S� verefica erro
      
       f_close(&fil);//Fecha o arquivo
      
}
        