#include "mcu_regs.h"
#include "uart.h"
#include "stdio.h"
#include "diskio.h"
#include "ff.h"

static FILINFO Finfo;
static FATFS Fatfs[1];
static uint8_t bufData[64];
//static uint8_t line[100];
static FIL fil;
static FRESULT fr;  /* FatFs return code */



void iniciaData(void);
int verificaSD();
int criaArquivo();